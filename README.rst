===============================
Aletheia
===============================


.. image:: https://img.shields.io/pypi/v/aletheia.svg
        :target: https://pypi.python.org/pypi/aletheia

.. image:: https://img.shields.io/travis/petrilli/aletheia.svg
        :target: https://travis-ci.org/petrilli/aletheia

.. image:: https://readthedocs.org/projects/aletheia/badge/?version=latest
        :target: https://aletheia.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/petrilli/aletheia/shield.svg
     :target: https://pyup.io/repos/github/petrilli/aletheia/
     :alt: Updates


Manage secrets in Google Cloud Platform


* Free software: BSD license
* Documentation: https://aletheia.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

